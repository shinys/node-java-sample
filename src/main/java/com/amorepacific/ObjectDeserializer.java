package com.amorepacific;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class ObjectDeserializer {
    public <T> T deserialize(final byte[] payload, final Class<T> valueType) throws IOException, ClassNotFoundException {
        ObjectInputStream oi = null;
        try{
            oi = new ObjectInputStream(new ByteArrayInputStream(payload));
            return valueType.cast(oi.readObject());
        }finally {
            if(oi!=null)
                oi.close();
        }
    }
}
