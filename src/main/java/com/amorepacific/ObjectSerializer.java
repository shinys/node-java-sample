package com.amorepacific;

import java.io.*;

public class ObjectSerializer {
    public byte[] serialize(final Object obj) throws IOException {
        ByteArrayOutputStream bo = null;
        ObjectOutputStream ou = null;
        try {
            bo = new ByteArrayOutputStream(512);
            ou = new ObjectOutputStream(bo);

            ou.writeObject(obj);
            ou.flush();
            return bo.toByteArray();
        }finally {
            if(ou != null)
                ou.close();

            if(bo != null)
                bo.close();
        }
    }
}
