package com.amorepacific;

import kr.ap.comm.member.vo.MemberSession;
import kr.ap.comm.support.tagging.TaggingInfo;
import net.g1project.ecp.api.model.ap.ap.ApMember;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        MemberSession memberSession = new MemberSession();
        ApMember apMember = new ApMember();
        apMember.setMemberId("100000");
        apMember.setEmployeeNo("AP35018876");
        apMember.setActivityPoint(3000);
        //apMember.setName(new net.g1project.ecp.api.model.EmbeddableName());

        memberSession.setMember(apMember);
        memberSession.setMember_sn(2345678L);
        memberSession.setLoginType(TaggingInfo.LoginType.FINGERPRINT);
        memberSession.setAccessToken("wekljrhwifho'wfvjfeoriuv'peroge'prtryouvvuioe");
        memberSession.setRefreshToken("dfiuwenflejwoeijnerogothekrm;wle");
        memberSession.setUser_ciNo("ci_no");
        memberSession.setUser_joinType("joinType");
        memberSession.setUser_incsNo("insc_no");


        ObjectSerializer os = new ObjectSerializer();
        byte[] serial = os.serialize(memberSession);


        ObjectDeserializer od = new ObjectDeserializer();
        MemberSession des = od.deserialize(serial, MemberSession.class);

        System.out.println(des);

    }
}
