package kr.ap.comm.support.tagging;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * GA/AA 태깅을 위한 정보
 */
public class TaggingInfo implements Serializable {

    /* 공통항목 */
    private String site = "undefined";
    private String siteName = "undefined";
    private ChannelType channel = ChannelType.PC;
    private String cid = "undefined"; // GA 에서 각 사용자에게 할당하는 CID 값

    /* 회원정보 */
    private Customer customer = new Customer();

    /* Page정보 */
    private Page page = new Page();

    /* 검색정보*/
    private Search search = new Search();

    /* 상품정보 */
    private Product prod = new Product();

    /* 장바구니 */
    private Cart cart = new Cart();

    /* 주문서 */
    private Order order = new Order();

    /* 주문완료 */
    private Purchase purchase = new Purchase();

    /* 주문취소 */
    private Refund refund = new Refund();

    /* objects */
    private List<Product> prodList = new ArrayList<>();
    private Payment payment = new Payment();

    public static class Customer implements Serializable {
        private String userId = "undefined";
        private String incsNo = "undefined";
        private String incsYn = "undefined"; // O, 비로그인시 undefined
        private String loginYn = "N";
        private LoginType loginType = LoginType.undefined;
        private String age = "undefined";
        private String birthYear = "undefined";
        private String birthDay = "undefined";
        private Gender gender = Gender.undefined;
        private String memberLevel = "undefined";
        private String incsNo_plain = "undefined";
        private String receiveEmailAgree = "undefined";
        private String receiveSmsAgree = "undefined";
        private String receiveDmAgree = "undefined";
        private String receiveTmAgree = "undefined";

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getIncsNo() {
            return incsNo;
        }

        public void setIncsNo(String incsNo) {
            this.incsNo = incsNo;
        }

        public String getIncsYn() {
            return incsYn;
        }

        public void setIncsYn(String incsYn) {
            this.incsYn = incsYn;
        }

        public String getLoginYn() {
            return loginYn;
        }

        public void setLoginYn(String loginYn) {
            this.loginYn = loginYn;
        }

        public LoginType getLoginType() {
            return loginType;
        }

        public void setLoginType(LoginType loginType) {
            this.loginType = loginType;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getBirthYear() {
            return birthYear;
        }

        public void setBirthYear(String birthYear) {
            this.birthYear = birthYear;
        }

        public String getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(String birthDay) {
            this.birthDay = birthDay;
        }

        public Gender getGender() {
            return gender;
        }

        public void setGender(Gender gender) {
            this.gender = gender;
        }

        public String getMemberLevel() {
            return memberLevel;
        }

        public void setMemberLevel(String memberLevel) {
            this.memberLevel = memberLevel;
        }

        public String getIncsNo_plain() {
            return incsNo_plain;
        }

        public void setIncsNo_plain(String incsNo_plain) {
            this.incsNo_plain = incsNo_plain;
        }

        public String getReceiveEmailAgree() {
            return receiveEmailAgree;
        }

        public void setReceiveEmailAgree(String receiveEmailAgree) {
            this.receiveEmailAgree = receiveEmailAgree;
        }

        public String getReceiveSmsAgree() {
            return receiveSmsAgree;
        }

        public void setReceiveSmsAgree(String receiveSmsAgree) {
            this.receiveSmsAgree = receiveSmsAgree;
        }

        public String getReceiveDmAgree() {
            return receiveDmAgree;
        }

        public void setReceiveDmAgree(String receiveDmAgree) {
            this.receiveDmAgree = receiveDmAgree;
        }

        public String getReceiveTmAgree() {
            return receiveTmAgree;
        }

        public void setReceiveTmAgree(String receiveTmAgree) {
            this.receiveTmAgree = receiveTmAgree;
        }
    }

    public enum LoginType {
        NORMAL, MOBILE, FINGERPRINT, AUTO, undefined, ORDER_NUMBER
    }

    public enum Gender {
        M, F, undefined
    }

    public enum ChannelType {
        PC, MOBILE, APP
    }

    public static class Page implements Serializable {
        private String countryCode = "KOR";
        private String langCode = "KO";
        private String name = "undefined";
        private String url = "undefined";

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getLangCode() {
            return langCode;
        }

        public void setLangCode(String langCode) {
            this.langCode = langCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Search implements Serializable {
        private String keyword = "undefined";
        private String result= "undefined"; // O,X
        private String type= "undefined"; // 직접입력(KEY_IN), 추천검색어(RECOMMEND), 최근검색어(RECENT), 인기검색어(POPULAR)
        private int resultCount;

        private List<Product> prodList = new ArrayList<>();

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getResultCount() {
            return resultCount;
        }

        public void setResultCount(int resultCount) {
            this.resultCount = resultCount;
        }

        public List<Product> getProdList() {
            return prodList;
        }

        public void setProdList(List<Product> prodList) {
            this.prodList = prodList;
        }
    }

    public static class Cart implements Serializable{
        private Payment payment = new Payment();
        private List<Product> prodList = new ArrayList<>();

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public List<Product> getProdList() {
            return prodList;
        }

        public void setProdList(List<Product> prodList) {
            this.prodList = prodList;
        }
    }

    public static class Order implements Serializable{
        private Payment payment = new Payment();
        private List<Product> prodList = new ArrayList<>();

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public List<Product> getProdList() {
            return prodList;
        }

        public void setProdList(List<Product> prodList) {
            this.prodList = prodList;
        }
    }

    public static class Purchase implements Serializable{
        private Payment payment = new Payment();
        private List<Product> prodList = new ArrayList<>();

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public List<Product> getProdList() {
            return prodList;
        }

        public void setProdList(List<Product> prodList) {
            this.prodList = prodList;
        }
    }

    public static class Refund implements Serializable {
        private String content = "undefined";
        private Payment payment = new Payment();
        private List<Product> prodList = new ArrayList<>();

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public List<Product> getProdList() {
            return prodList;
        }

        public void setProdList(List<Product> prodList) {
            this.prodList = prodList;
        }
    }

    public static class Product implements Serializable {

        /* AP GA Tagging */
        private String name = "undefined";       			//상품명
        private String code = "undefined";					//제품온라인코드
        private String brand = "undefined";					//브랜드
        private BigDecimal prd_price = BigDecimal.ZERO;		//할인전 가격
        private BigDecimal price = BigDecimal.ZERO;			//가격
        private int quantity = 0;							//수량
        private String variant = "undefined";				//옵션
        private String promotion = "undefined";				//프로모션명
        private String cate = "undefined";					//카테고리명
        private String catecode = "undefined";				//카테고리코드
        /* AP GA Tagging */

        private String sku = "undefined";
        //private String name = "undefined";
        //private String brand = "undefined";
        //private BigDecimal price = BigDecimal.ZERO;
        //private int quantity = 0;
        //private String variant = "undefined"; // 제품 옵션, 단일옵션인경우 '옵션없음'
        private String coupon = "undefined"; // 쿠폰코드_쿠폰명, 특정 상품/카테고리/브랜드에 적용되는 쿠폰
        private String shippingType = "Normal";
        private String sapCode = "undefined";
        private BigDecimal beautyPoint = BigDecimal.ZERO;
        //private String promotion = "undefined";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public BigDecimal getPrd_price() {
            return prd_price;
        }

        public void setPrd_price(BigDecimal prd_price) {
            this.prd_price = prd_price;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getVariant() {
            return variant;
        }

        public void setVariant(String variant) {
            this.variant = variant;
        }

        public String getPromotion() {
            return promotion;
        }

        public void setPromotion(String promotion) {
            this.promotion = promotion;
        }

        public String getCate() {
            return cate;
        }

        public void setCate(String cate) {
            this.cate = cate;
        }

        public String getCatecode() {
            return catecode;
        }

        public void setCatecode(String catecode) {
            this.catecode = catecode;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getCoupon() {
            return coupon;
        }

        public void setCoupon(String coupon) {
            this.coupon = coupon;
        }

        public String getShippingType() {
            return shippingType;
        }

        public void setShippingType(String shippingType) {
            this.shippingType = shippingType;
        }

        public String getSapCode() {
            return sapCode;
        }

        public void setSapCode(String sapCode) {
            this.sapCode = sapCode;
        }

        public BigDecimal getBeautyPoint() {
            return beautyPoint;
        }

        public void setBeautyPoint(BigDecimal beautyPoint) {
            this.beautyPoint = beautyPoint;
        }
    }

    public static class Payment implements Serializable {

        /* AP GA Tagging */
        private BigDecimal ap_price = BigDecimal.ZERO;			//총 결제금액
        private BigDecimal ap_prod_price = BigDecimal.ZERO;		//총 상품금액
        private BigDecimal ap_dc_total = BigDecimal.ZERO;		//총 할인금액
        private BigDecimal ap_dc_basic = BigDecimal.ZERO;		//상품할인
        private BigDecimal ap_dc_coupon = BigDecimal.ZERO;		//쿠폰할인
        private BigDecimal ap_dc_membership = BigDecimal.ZERO;	//멤버쉽할인(회원등급할인)
        private BigDecimal ap_giftcard = BigDecimal.ZERO;		//기프트카드 사용금액
        private BigDecimal ap_point = BigDecimal.ZERO;			//뷰티포인트 사용금액
        private BigDecimal ap_online_gift = BigDecimal.ZERO;	//온라인상품권 사용금액
        private String ap_orderNo = "undefined";				//주문번호
        private BigDecimal ap_beauty_acc = BigDecimal.ZERO;		//뷰티보인트 적립액
        private BigDecimal ap_shipping = BigDecimal.ZERO;		//배송비
        private String ap_pay_type = "undefined";				//결제수단
        private List<String> ap_coupon_names = new ArrayList<>();			//쿠폰명
        /* AP GA Tagging */

        private String checkoutOption = "undefined"; // 결제수단
        private String orderNo = "undefined"; // 주문번호
        private BigDecimal amount = BigDecimal.ZERO; // 최종결제 금액
        private BigDecimal tax = BigDecimal.ZERO;
        private BigDecimal shippingFee = BigDecimal.ZERO;
        private String coupon = "undefined"; // 쿠폰코드_쿠폰명, 장바구니 쿠폰
        private String detailPaymentMethod = "undefined"; // 은행명 or 카드명, 결제수단_카드명

        public BigDecimal getAp_price() {
            return ap_price;
        }

        public void setAp_price(BigDecimal ap_price) {
            this.ap_price = ap_price;
        }

        public BigDecimal getAp_prod_price() {
            return ap_prod_price;
        }

        public void setAp_prod_price(BigDecimal ap_prod_price) {
            this.ap_prod_price = ap_prod_price;
        }

        public BigDecimal getAp_dc_total() {
            return ap_dc_total;
        }

        public void setAp_dc_total(BigDecimal ap_dc_total) {
            this.ap_dc_total = ap_dc_total;
        }

        public BigDecimal getAp_dc_basic() {
            return ap_dc_basic;
        }

        public void setAp_dc_basic(BigDecimal ap_dc_basic) {
            this.ap_dc_basic = ap_dc_basic;
        }

        public BigDecimal getAp_dc_coupon() {
            return ap_dc_coupon;
        }

        public void setAp_dc_coupon(BigDecimal ap_dc_coupon) {
            this.ap_dc_coupon = ap_dc_coupon;
        }

        public BigDecimal getAp_dc_membership() {
            return ap_dc_membership;
        }

        public void setAp_dc_membership(BigDecimal ap_dc_membership) {
            this.ap_dc_membership = ap_dc_membership;
        }

        public BigDecimal getAp_giftcard() {
            return ap_giftcard;
        }

        public void setAp_giftcard(BigDecimal ap_giftcard) {
            this.ap_giftcard = ap_giftcard;
        }

        public BigDecimal getAp_point() {
            return ap_point;
        }

        public void setAp_point(BigDecimal ap_point) {
            this.ap_point = ap_point;
        }

        public BigDecimal getAp_online_gift() {
            return ap_online_gift;
        }

        public void setAp_online_gift(BigDecimal ap_online_gift) {
            this.ap_online_gift = ap_online_gift;
        }

        public String getAp_orderNo() {
            return ap_orderNo;
        }

        public void setAp_orderNo(String ap_orderNo) {
            this.ap_orderNo = ap_orderNo;
        }

        public BigDecimal getAp_beauty_acc() {
            return ap_beauty_acc;
        }

        public void setAp_beauty_acc(BigDecimal ap_beauty_acc) {
            this.ap_beauty_acc = ap_beauty_acc;
        }

        public BigDecimal getAp_shipping() {
            return ap_shipping;
        }

        public void setAp_shipping(BigDecimal ap_shipping) {
            this.ap_shipping = ap_shipping;
        }

        public String getAp_pay_type() {
            return ap_pay_type;
        }

        public void setAp_pay_type(String ap_pay_type) {
            this.ap_pay_type = ap_pay_type;
        }

        public List<String> getAp_coupon_names() {
            return ap_coupon_names;
        }

        public void setAp_coupon_names(List<String> ap_coupon_names) {
            this.ap_coupon_names = ap_coupon_names;
        }

        public String getCheckoutOption() {
            return checkoutOption;
        }

        public void setCheckoutOption(String checkoutOption) {
            this.checkoutOption = checkoutOption;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public BigDecimal getTax() {
            return tax;
        }

        public void setTax(BigDecimal tax) {
            this.tax = tax;
        }

        public BigDecimal getShippingFee() {
            return shippingFee;
        }

        public void setShippingFee(BigDecimal shippingFee) {
            this.shippingFee = shippingFee;
        }

        public String getCoupon() {
            return coupon;
        }

        public void setCoupon(String coupon) {
            this.coupon = coupon;
        }

        public String getDetailPaymentMethod() {
            return detailPaymentMethod;
        }

        public void setDetailPaymentMethod(String detailPaymentMethod) {
            this.detailPaymentMethod = detailPaymentMethod;
        }
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public ChannelType getChannel() {
        return channel;
    }

    public void setChannel(ChannelType channel) {
        this.channel = channel;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Search getSearch() {
        return search;
    }

    public void setSearch(Search search) {
        this.search = search;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public Product getProd() {
        return prod;
    }

    public void setProd(Product prod) {
        this.prod = prod;
    }

    public Refund getRefund() {
        return refund;
    }

    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    public List<Product> getProdList() {
        return prodList;
    }

    public void setProdList(List<Product> prodList) {
        this.prodList = prodList;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
