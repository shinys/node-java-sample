package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;

/**
 * 회원가입약관동의여부
 */
public class SignupTermsAgree implements Serializable {

    /**
     * 약관전시코드
     */
    private String termsDisplayCode;

    /**
     * 약관동의여부
     */
    private String agreeYn;

    /**
     * 약관전시코드
     */
    public String getTermsDisplayCode() {
        return termsDisplayCode;
    }

    /**
     * 약관전시코드
     */
    public void setTermsDisplayCode(String termsDisplayCode) {
        this.termsDisplayCode = termsDisplayCode;
    }

    /**
     * 약관동의여부
     */
    public String getAgreeYn() {
        return agreeYn;
    }

    /**
     * 약관동의여부
     */
    public void setAgreeYn(String agreeYn) {
        this.agreeYn = agreeYn;
    }
}
