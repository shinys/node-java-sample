package net.g1project.ecp.api.model;

import java.io.Serializable;

public class EmbeddableName implements Serializable {
    private String name1;
    private String name2;
    private String name3;
    private String name4;

    public EmbeddableName() {
    }

    public String getName1() {
        return this.name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return this.name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return this.name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return this.name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }
}
