package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;

/**
 * 멤버십
 */
public class MemberMembership implements Serializable {

    /**
     * 멤버십서비스코드
     */
    private String membershipServiceCode;

    /**
     * 멤버십회원식별자
     */
    private String membershipMemberIdentifier;

    /**
     * 멤버십서비스코드
     */
    public String getMembershipServiceCode() {
        return membershipServiceCode;
    }

    /**
     * 멤버십서비스코드
     */
    public void setMembershipServiceCode(String membershipServiceCode) {
        this.membershipServiceCode = membershipServiceCode;
    }

    /**
     * 멤버십회원식별자
     */
    public String getMembershipMemberIdentifier() {
        return membershipMemberIdentifier;
    }

    /**
     * 멤버십회원식별자
     */
    public void setMembershipMemberIdentifier(String membershipMemberIdentifier) {
        this.membershipMemberIdentifier = membershipMemberIdentifier;
    }
}
