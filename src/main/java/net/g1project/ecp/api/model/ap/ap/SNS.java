package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;
import java.util.Date;

/**
 * SNS연계정보
 */
public class SNS implements Serializable {

    /**
     * SNS코드
     */
    private String snsCode;

    /**
     * SNS연계일
     */
    private Date connDt;

    /**
     * SNS코드
     */
    public String getSnsCode() {
        return snsCode;
    }

    /**
     * SNS코드
     */
    public void setSnsCode(String snsCode) {
        this.snsCode = snsCode;
    }

    /**
     * SNS연계일
     */
    public Date getConnDt() {
        return connDt;
    }

    /**
     * SNS연계일
     */
    public void setConnDt(Date connDt) {
        this.connDt = connDt;
    }
}
