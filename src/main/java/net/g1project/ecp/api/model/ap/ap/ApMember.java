package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

/**
 * 회원 정보
 */
public class ApMember implements Serializable {

    private static final long serialVersionUID = 2524970897567676407L;

    /**
     * 회원아이디
     */
    private String memberId;

    /**
     * 회원등급명
     */
    private String memberLevelName;

    /**
     * SNS연계대상
     */
    private List<SNS> SNSIf;

    /**
     * 임직원여부
     */
    private String employeeYn;

    /**
     * 임직원사원번호
     */
    private String employeeNo;

    /**
     * 임직원유형코드
     */
    private String employeeTypeCode;

    /**
     * 협력사코드
     */
    private String partnersCode;

    /**
     * 약관동의목록
     */
    private List<SignupTermsAgree> memberTermsAgrees;

    /**
     * 회원명
     */
    private EmbeddableName name;

    /**
     * 전화번호1
     */
    private EmbeddableTel phoneNo1;

    /**
     * 전화번호1이 발신번호 사전등록된 번호인지 여부
     */
    private Boolean advanceRegistrationPhoneNo;

    /**
     * 전화번호2
     */
    private EmbeddableTel phoneNo2;

    /**
     * 주소
     */
    private EmbeddableAddress address;

    /**
     * 이메일주소
     */
    private String emailAddress;

    /**
     * 회원가입일자
     */
    private Date memberSignupDt;

    /**
     * 양력/음력
     */
    private String solarLunarCode;

    /**
     * 생년월일년
     */
    private String dobYear;

    /**
     * 생년월일월
     */
    private String dobMonth;

    /**
     * 생년월일일
     */
    private String dobDay;

    /**
     * 성별코드
     */
    private String genderCode;

    /**
     * 유형별 수신동의여부
     */
    private List<SignupReceiveAgree> memberReceiveAgrees;

    /**
     * 가입추천회원아이디
     */
    private String signupRecommendMemberId;

    /**
     * 활동포인트
     */
    private Integer activityPoint;

    /**
     * 회원멤버십
     */
    private List<MemberMembership> memberMembership;

    /**
     * 회원부가속성
     */
    private List<MemberAddAttr> memberAddAttr;

    /**
     * 통합회원번호
     */
    private String incsNo;

    /**
     * POS회원식별자
     */
    private String posMemberIdentifier;

    /**
     * 유료회원여부
     */
    private String payMemberYn;

    /**
     * 구회원아이디
     */
    private String oldMemberId;

    /**
     * 회원아이디
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 회원아이디
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     * 회원등급명
     */
    public String getMemberLevelName() {
        return memberLevelName;
    }

    /**
     * 회원등급명
     */
    public void setMemberLevelName(String memberLevelName) {
        this.memberLevelName = memberLevelName;
    }

    /**
     * SNS연계대상
     */
    public List<SNS> getSNSIf() {
        return SNSIf;
    }

    /**
     * SNS연계대상
     */
    public void setSNSIf(List<SNS> SNSIf) {
        this.SNSIf = SNSIf;
    }

    /**
     * 임직원여부
     */
    public String getEmployeeYn() {
        return employeeYn;
    }

    /**
     * 임직원여부
     */
    public void setEmployeeYn(String employeeYn) {
        this.employeeYn = employeeYn;
    }

    /**
     * 임직원사원번호
     */
    public String getEmployeeNo() {
        return employeeNo;
    }

    /**
     * 임직원사원번호
     */
    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    /**
     * 임직원유형코드
     */
    public String getEmployeeTypeCode() {
        return employeeTypeCode;
    }

    /**
     * 임직원유형코드
     */
    public void setEmployeeTypeCode(String employeeTypeCode) {
        this.employeeTypeCode = employeeTypeCode;
    }

    /**
     * 협력사코드
     */
    public String getPartnersCode() {
        return partnersCode;
    }

    /**
     * 협력사코드
     */
    public void setPartnersCode(String partnersCode) {
        this.partnersCode = partnersCode;
    }

    /**
     * 약관동의목록
     */
    public List<SignupTermsAgree> getMemberTermsAgrees() {
        return memberTermsAgrees;
    }

    /**
     * 약관동의목록
     */
    public void setMemberTermsAgrees(List<SignupTermsAgree> memberTermsAgrees) {
        this.memberTermsAgrees = memberTermsAgrees;
    }

    /**
     * 회원명
     */
    public EmbeddableName getName() {
        return name;
    }

    /**
     * 회원명
     */
    public void setName(EmbeddableName name1) {
        this.name = name1;
    }

    /**
     * 전화번호1
     */
    public EmbeddableTel getPhoneNo1() {
        return phoneNo1;
    }

    /**
     * 전화번호1
     */
    public void setPhoneNo1(EmbeddableTel phoneNo1) {
        phoneNo1 = phoneNo1;
    }

    /**
     * 전화번호1이 발신번호 사전등록된 번호인지 여부
     */
    public Boolean isAdvanceRegistrationPhoneNo() {
        return advanceRegistrationPhoneNo;
    }

    /**
     * 전화번호1이 발신번호 사전등록된 번호인지 여부
     */
    public void setAdvanceRegistrationPhoneNo(Boolean advanceRegistrationPhoneNo) {
        this.advanceRegistrationPhoneNo = advanceRegistrationPhoneNo;
    }

    /**
     * 전화번호2
     */
    public EmbeddableTel getPhoneNo2() {
        return phoneNo2;
    }

    /**
     * 전화번호2
     */
    public void setPhoneNo2(EmbeddableTel phoneNo2) {
        phoneNo2 = phoneNo2;
    }

    /**
     * 주소
     */
    public EmbeddableAddress getAddress() {
        return address;
    }

    /**
     * 주소
     */
    public void setAddress(EmbeddableAddress address) {
        address = address;
    }

    /**
     * 이메일주소
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * 이메일주소
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * 회원가입일자
     */
    public Date getMemberSignupDt() {
        return memberSignupDt;
    }

    /**
     * 회원가입일자
     */
    public void setMemberSignupDt(Date memberSignupDt) {
        this.memberSignupDt = memberSignupDt;
    }

    /**
     * 양력/음력
     */
    public String getSolarLunarCode() {
        return solarLunarCode;
    }

    /**
     * 양력/음력
     */
    public void setSolarLunarCode(String solarLunarCode) {
        this.solarLunarCode = solarLunarCode;
    }

    /**
     * 생년월일년
     */
    public String getDobYear() {
        return dobYear;
    }

    /**
     * 생년월일년
     */
    public void setDobYear(String dobYear) {
        this.dobYear = dobYear;
    }

    /**
     * 생년월일월
     */
    public String getDobMonth() {
        return dobMonth;
    }

    /**
     * 생년월일월
     */
    public void setDobMonth(String dobMonth) {
        this.dobMonth = dobMonth;
    }

    /**
     * 생년월일일
     */
    public String getDobDay() {
        return dobDay;
    }

    /**
     * 생년월일일
     */
    public void setDobDay(String dobDay) {
        this.dobDay = dobDay;
    }

    /**
     * 성별코드
     */
    public String getGenderCode() {
        return genderCode;
    }

    /**
     * 성별코드
     */
    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    /**
     * 유형별 수신동의여부
     */
    public List<SignupReceiveAgree> getMemberReceiveAgrees() {
        return memberReceiveAgrees;
    }

    /**
     * 유형별 수신동의여부
     */
    public void setMemberReceiveAgrees(List<SignupReceiveAgree> memberReceiveAgrees) {
        this.memberReceiveAgrees = memberReceiveAgrees;
    }

    /**
     * 가입추천회원아이디
     */
    public String getSignupRecommendMemberId() {
        return signupRecommendMemberId;
    }

    /**
     * 가입추천회원아이디
     */
    public void setSignupRecommendMemberId(String signupRecommendMemberId) {
        this.signupRecommendMemberId = signupRecommendMemberId;
    }

    /**
     * 활동포인트
     */
    public Integer getActivityPoint() {
        return activityPoint;
    }

    /**
     * 활동포인트
     */
    public void setActivityPoint(Integer activityPoint) {
        this.activityPoint = activityPoint;
    }

    /**
     * 회원멤버십
     */
    public List<MemberMembership> getMemberMembership() {
        return memberMembership;
    }

    /**
     * 회원멤버십
     */
    public void setMemberMembership(List<MemberMembership> memberMembership) {
        this.memberMembership = memberMembership;
    }

    /**
     * 회원부가속성
     */
    public List<MemberAddAttr> getMemberAddAttr() {
        return memberAddAttr;
    }

    /**
     * 회원부가속성
     */
    public void setMemberAddAttr(List<MemberAddAttr> memberAddAttr) {
        this.memberAddAttr = memberAddAttr;
    }

    /**
     * 통합회원번호
     */
    public String getIncsNo() {
        return incsNo;
    }

    /**
     * 통합회원번호
     */
    public void setIncsNo(String incsNo) {
        this.incsNo = incsNo;
    }

    /**
     * POS회원식별자
     */
    public String getPosMemberIdentifier() {
        return posMemberIdentifier;
    }

    /**
     * POS회원식별자
     */
    public void setPosMemberIdentifier(String posMemberIdentifier) {
        this.posMemberIdentifier = posMemberIdentifier;
    }

    /**
     * 유료회원여부
     */
    public String getPayMemberYn() {
        return payMemberYn;
    }

    /**
     * 유료회원여부
     */
    public void setPayMemberYn(String payMemberYn) {
        this.payMemberYn = payMemberYn;
    }

    /**
     * 구회원아이디
     */
    public String getOldMemberId() {
        return oldMemberId;
    }

    /**
     * 구회원아이디
     */
    public void setOldMemberId(String oldMemberId) {
        this.oldMemberId = oldMemberId;
    }
}
