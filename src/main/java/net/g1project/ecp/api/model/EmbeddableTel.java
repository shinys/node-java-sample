package net.g1project.ecp.api.model;

import java.io.Serializable;

public class EmbeddableTel implements Serializable {

    /**
     * 국가번호
     */
    private String countryNo;

    /**
     * 전화번호
     */
    private String phoneNo;

    /**
     * 국가번호
     */
    public String getCountryNo() {
        return countryNo;
    }

    /**
     * 국가번호
     */
    public void setCountryNo(String countryNo) {
        this.countryNo = countryNo;
    }

    /**
     * 전화번호
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * 전화번호
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
