package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;

/**
 * 회원부가속성
 */
public class MemberAddAttr implements Serializable {

    /**
     * 회원속성코드 (피부타입, 취미 등)
     */
    private String memberAddAttrCode;

    /**
     * 회원부가속성코드 (건성,지성..)
     */
    private String memberAddAttrValCode;

    /**
     * 회원속성이름 (피부타입, 취미 등)
     */
    private String memberAddAttrCodeName;

    /**
     * 회원부가속성이름 (건성,지성..)
     */
    private String memberAddAttrValCodeName;

    /**
     * 회원속성코드 (피부타입, 취미 등)
     */
    public String getMemberAddAttrCode() {
        return memberAddAttrCode;
    }

    /**
     * 회원속성코드 (피부타입, 취미 등)
     */
    public void setMemberAddAttrCode(String memberAddAttrCode) {
        this.memberAddAttrCode = memberAddAttrCode;
    }

    /**
     * 회원부가속성코드 (건성,지성..)
     */
    public String getMemberAddAttrValCode() {
        return memberAddAttrValCode;
    }

    /**
     * 회원부가속성코드 (건성,지성..)
     */
    public void setMemberAddAttrValCode(String memberAddAttrValCode) {
        this.memberAddAttrValCode = memberAddAttrValCode;
    }

    /**
     * 회원속성이름 (피부타입, 취미 등)
     */
    public String getMemberAddAttrCodeName() {
        return memberAddAttrCodeName;
    }

    /**
     * 회원속성이름 (피부타입, 취미 등)
     */
    public void setMemberAddAttrCodeName(String memberAddAttrCodeName) {
        this.memberAddAttrCodeName = memberAddAttrCodeName;
    }

    /**
     * 회원부가속성이름 (건성,지성..)
     */
    public String getMemberAddAttrValCodeName() {
        return memberAddAttrValCodeName;
    }

    /**
     * 회원부가속성이름 (건성,지성..)
     */
    public void setMemberAddAttrValCodeName(String memberAddAttrValCodeName) {
        this.memberAddAttrValCodeName = memberAddAttrValCodeName;
    }
}
