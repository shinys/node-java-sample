package net.g1project.ecp.api.model.ap.ap;

import net.g1project.ecp.api.model.*;
import java.io.Serializable;

/**
 * 유형별 수신동의 여부
 */
public class SignupReceiveAgree implements Serializable {

    /**
     * 수신동의유형
     */
    private String receiveTypeCode;

    /**
     * 동의여부코드
     */
    private String receiveAgreeCode;

    /**
     * 수신동의유형
     */
    public String getReceiveTypeCode() {
        return receiveTypeCode;
    }

    /**
     * 수신동의유형
     */
    public void setReceiveTypeCode(String receiveTypeCode) {
        this.receiveTypeCode = receiveTypeCode;
    }

    /**
     * 동의여부코드
     */
    public String getReceiveAgreeCode() {
        return receiveAgreeCode;
    }

    /**
     * 동의여부코드
     */
    public void setReceiveAgreeCode(String receiveAgreeCode) {
        this.receiveAgreeCode = receiveAgreeCode;
    }
}
