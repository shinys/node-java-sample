package com.amorepacific;

import kr.ap.comm.member.vo.MemberSession;
import net.g1project.ecp.api.model.*;
import kr.ap.comm.support.tagging.TaggingInfo;
import net.g1project.ecp.api.model.ap.ap.ApMember;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class ObjectTest {

    MemberSession orignalMemberSession = null;

    @BeforeEach
    public void setup(){
        ApMember apMember = new ApMember();
        apMember.setMemberId("100000");
        apMember.setEmployeeNo("AP35018876");
        apMember.setActivityPoint(3000);
        apMember.setName(new EmbeddableName());

        orignalMemberSession = new MemberSession();
        orignalMemberSession.setMember(apMember);
        orignalMemberSession.setMember_sn(2345678L);
        orignalMemberSession.setLoginType(TaggingInfo.LoginType.FINGERPRINT);
        orignalMemberSession.setAccessToken("wekljrhwifho'wfvjfeoriuv'peroge'prtryouvvuioe");
        orignalMemberSession.setRefreshToken("dfiuwenflejwoeijnerogothekrm;wle");
        orignalMemberSession.setUser_ciNo("ci_no");
        orignalMemberSession.setUser_joinType("joinType");
        orignalMemberSession.setUser_incsNo("insc_no");
    }

    @Test
    public void 객체직렬화() throws IOException, ClassNotFoundException {

        ObjectSerializer os = new ObjectSerializer();
        byte[] bytesOfMemberSession = os.serialize(orignalMemberSession);


        ObjectDeserializer od = new ObjectDeserializer();
        MemberSession deserilaizedMemberSession = od.deserialize(bytesOfMemberSession, MemberSession.class);

        // 직렬화 전 후의 두 MemberSession 객체는 다른 인스턴스임을 확인
        Assertions.assertNotEquals(orignalMemberSession, deserilaizedMemberSession);

        // 두 인스턴스 멤버 변수 값 일치 여부 확인
        Assertions.assertEquals(orignalMemberSession.getMember_sn(), deserilaizedMemberSession.getMember_sn());

    }


    @Test
    public void 이것도되나() throws IOException, ClassNotFoundException {

        ObjectSerializer os = new ObjectSerializer();
        byte[] bytesOfMemberSession = os.serialize(orignalMemberSession);

        ObjectDeserializer od = new ObjectDeserializer();
        MemberSession deserilaizedMemberSession = od.deserialize(bytesOfMemberSession, new MemberSession().getClass());

        // 직렬화 전 후의 두 MemberSession 객체는 다른 인스턴스임을 확인
        Assertions.assertNotEquals(orignalMemberSession, deserilaizedMemberSession);

        // 두 인스턴스 멤버 변수 값 일치 여부 확인
        Assertions.assertEquals(orignalMemberSession.getMember_sn(), deserilaizedMemberSession.getMember_sn());

    }
}
