> https://github.com/joeferner/node-java 문서를 참고하여 작성했습니다.  
> 이 문서에는 node에 java 모듈 올리는건 기술하지 않았으며 node에서 MemberSession 얻는 java 코드 호출하는 방법에 집중합니다.      
  
### Build
`$ gradlew clean build`  
빌드 결과는 build/libs/deserializer-1.0-RELEASE.jar 입니다.  
byte[]를 java 객체로 역직렬화 합니다.    

 
### Java에서는 다음과 같이 사용 합니다.

직렬화 / 역직렬화  
`com.amorepacific.ObjectSerializer`, `com.amorepacific.ObjectDeserializer` 를 통해 수행.  
~~~java
// 직렬화할 임의의 MemberSession 객체 생성
ApMember apMember = new ApMember();
apMember.setMemberId("100000");
apMember.setEmployeeNo("AP35018876");
apMember.setActivityPoint(3000);
apMember.setName(new EmbeddableName());
MemberSession orignalMemberSession = new MemberSession();
orignalMemberSession.setMember(apMember);
orignalMemberSession.setMember_sn(2345678L);
orignalMemberSession.setLoginType(TaggingInfo.LoginType.FINGERPRINT);
orignalMemberSession.setAccessToken("wekljrhwifho'wfvjfeoriuv'peroge'prtryouvvuioe");
orignalMemberSession.setRefreshToken("dfiuwenflejwoeijnerogothekrm;wle");
orignalMemberSession.setUser_ciNo("ci_no");
orignalMemberSession.setUser_joinType("joinType");
orignalMemberSession.setUser_incsNo("insc_no");

// 객체를 byte[]로 직렬화. 이는 redis에 저장된 value에 해당 함.  
ObjectSerializer os = new ObjectSerializer();
byte[] bytesOfMemberSession = os.serialize(orignalMemberSession);

// 직렬화된 byte[]를 MemberSession 객체로 역직렬화.
ObjectDeserializer od = new ObjectDeserializer();
MemberSession deserilaizedMemberSession = od.deserialize(bytesOfMemberSession, MemberSession.class);
~~~


### node.js에서 대략 다음 처럼하면 되지 않을까해요. (테스트는 못해봤습니다.)  
java module 초기화  
~~~typescript
"use strict";
var fs = require("fs");
var java = require("java");
// deserializer-1.0-RELEASE.jar 경로
var baseDir = "./target/dependency";
var dependencies = fs.readdirSync(baseDir);
dependencies.forEach(function(dependency){
    java.classpath.push(baseDir + "/" + dependency);
})
java.options.push('-Djava.awt.headless=true');
java.options.push('-Xms128m');
java.options.push('-Xmx128m');
java.options.push('-XX:MetaspaceSize=128M');
java.options.push('-XX:MaxMetaspaceSize=128M');
java.options.push('-Dfile.encoding=utf-8');

exports.getJavaInstance = function() {
    return java;
}
~~~

직렬화된 데이터는 node용 redis 모듈통해서 가져왔다고 가정합니다.
~~~typescript
var javaInit = require('./javaInit');
var java = javaInit.getJavaInstance();

// redis에서 직렬화된 memberSession bytes를 가져왔다고 가정.
const bytes = [
    104, 116, 116, 112, 115,
    58, 47, 47, 119, 119,
    119, 46, 106, 118, 116,
    46, 109, 101
];
const buffer = Buffer.from(bytes);
//buffer에서 byte[] 얻는 건 다음이 맞나? 이 부분은 확신이 없습니다.
var arrByte= new Uint8Array.from(buffer);


// 가져온 byte[]를 MemberSession 객체로 복원.
try {
    
    // 역직렬화 수행용 java class
    // 얘는 매번 생성할 필요 없이 한번 생성해서 계속 사용하면 됨.
    var objectDeserializer = java.newInstanceSync("com.amorepacific.ObjectDeserializer");
    // 역직렬화로 변환할 java type을 명시하기위한 객체
    // 얘도 매번 생성할 필요 없이 한번 생성해서 계속 사용하면 됨.
    var memberSession = java.newInstanceSync("kr.ap.comm.member.vo.MemberSession");
    
    
    
    // objectDeserializer.deserialize(bytesOfMemberSession, MemberSession.class) 호출. 
    // 리턴값 result 이 MemberSession 객체임.
    var result = java.callMethodSync(objectDeserializer, "deserialize" , arrByte , memberSession.getClassSync());
    
    console.log(result);
    console.log(result.getMember_snSync());
    console.log(result.getAccessTokenSync());
    console.log(result.getRefreshTokenSync());
    console.log(result.getUser_certNumSync());
    console.log(result.getUser_ciNoSync());

} catch(ex) {
    console.log(ex.cause.getMessageSync());
}
~~~